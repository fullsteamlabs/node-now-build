FROM node:lts-slim

RUN apt-get update && apt-get install -y --no-install-recommends apt-transport-https build-essential ca-certificates wget gnupg python \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
	  && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
	  && apt-get update && apt-get install -y --no-install-recommends \
	  google-chrome-stable \
	  fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf
RUN	apt-get update && apt-get install -y git ssh
RUN npm install -g now@17.0.4
RUN npm install -g vercel
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
RUN npm install -g puppeteer@1.19.0

WORKDIR /opt/app
USER node
WORKDIR /home/node

#!/usr/bin/env bash

docker build --no-cache -t fullsteamlabs/node-now-build .
docker push fullsteamlabs/node-now-build
